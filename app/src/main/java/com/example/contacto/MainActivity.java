package com.example.contacto;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    final ArrayList<Contactos> contactos = new ArrayList<Contactos>();
    private EditText txtNombre;
    private EditText txtTelefono;
    private EditText txtTelefono2;
    private EditText txtDireccion;
    private EditText txtNotas;
    private CheckBox chckFavorito;
    Contactos saveContact;
    int savedIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = findViewById(R.id.txtNombre);
        txtTelefono = findViewById(R.id.txtTel1);
        txtTelefono2 = findViewById(R.id.txtTel2);
        txtDireccion = findViewById(R.id.txtDir);
        txtNotas = findViewById(R.id.txtNotas);
        chckFavorito = findViewById(R.id.chkFav);
        Button btnGuardar = findViewById(R.id.btnGuardar);
        final Button btnLimpiar = findViewById(R.id.btnLimpiar);
        Button btnListar = findViewById(R.id.btnListar);
        Button btnSalir = findViewById(R.id.btnSalir);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Nombre = txtNombre.getText().toString();
                String Tel = txtTelefono.getText().toString();
                String Dir = txtDireccion.getText().toString();
                if (Nombre.matches("") || Tel.matches("") || Dir.matches("")) {
                    Toast.makeText(MainActivity.this, R.string.msgError, Toast.LENGTH_SHORT).show();
                } else {
                    Contactos nContacto = new Contactos();
                    int index = contactos.size();
                    if (saveContact !=null){
                        contactos.remove(savedIndex);
                        nContacto = saveContact;
                        index = savedIndex;
                    }
                    nContacto.setNombre(txtNombre.getText().toString());
                    nContacto.setTelefono1(txtTelefono.getText().toString());
                    nContacto.setTelefono2(txtTelefono2.getText().toString());
                    nContacto.setDomicilio(txtDireccion.getText().toString());
                    nContacto.setNotas(txtNotas.getText().toString());
                    nContacto.setFavorito(chckFavorito.isChecked());
                    contactos.add(index, nContacto);
                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    saveContact=null;
                    limpiar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos",contactos);
                i.putExtras(bObject);
                startActivityForResult(i,0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    protected void onActivityResult (int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode,resultCode,intent);
        if (intent !=null){
            Bundle oBundle = intent.getExtras();

            saveContact = (Contactos)oBundle.getSerializable("contacto");
            savedIndex = oBundle.getInt("index");
            txtNombre.setText(saveContact.getNombre());
            txtTelefono.setText(saveContact.getTelefono1());
            txtTelefono2.setText(saveContact.getTelefono2());
            txtNotas.setText(saveContact.getNotas());
            txtDireccion.setText(saveContact.getDomicilio());
            chckFavorito.setChecked(saveContact.isFavorito());
        }
        else{
            limpiar();
        }
    }

    public void limpiar(){
        saveContact=null;
        txtNombre.setText("");
        txtTelefono.setText("");
        txtTelefono2.setText("");
        txtDireccion.setText("");
        txtNotas.setText("");
        chckFavorito.setChecked(false);
    }
}



